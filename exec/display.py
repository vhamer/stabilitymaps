import random
import numpy as np
import matplotlib.patches as patches
import matplotlib
import sys
import tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from scipy.stats import rankdata
import sparse
import webbrowser

palette = [
  "#7FC97F" ,"#BEAED4" ,"#FDC086" ,"#FFFF99" ,"#386CB0" ,"#F0027F" ,"#BF5B17" ,"#666666" ,"#1B9E77"  ,"#7570B3"
  ,"#E6AB02" ,"#A6761D"  ,"#A6CEE3" ,"#1F78B4" ,"#B2DF8A" ,"#33A02C" ,"#FB9A99" ,"#E31A1C" ,"#FDBF6F" ,"#FF7F00" ,"#6A3D9A"
  ,"#FFFF99" ,"#B15928" ,"#FBB4AE" ,"#B3CDE3" ,"#CCEBC5" ,"#DECBE4" ,"#FED9A6" ,"#FFFFCC" ,"#E5D8BD"  ,"#F2F2F2" ,"#B3E2CD" ,"#FDCDAC"
  ,"#CBD5E8" ,"#F4CAE4" ,"#E6F5C9" ,"#FFF2AE" ,"#F1E2CC" ,"#CCCCCC" ,"#377EB8"  ,"#984EA3" ,"#FF7F00" ,"#FFFF33" ,"#A65628"
  ,"#F781BF" ,"#999999" ,"#66C2A5" ,"#FC8D62" ,"#8DA0CB" ,"#E78AC3" ,"#A6D854" ,"#FFD92F" ,"#E5C494" ,"#B3B3B3" ,"#8DD3C7" ,"#FFFFB3" ,"#BEBADA"
  ,"#FB8072" ,"#80B1D3" ,"#FDB462" ,"#B3DE69" ,"#FCCDE5" ,"#D9D9D9" ,"#BC80BD" ,"#CCEBC5" ,"#FFED6F", "#E7298A", "#D95F02", "#66A61E", "#4DAF4A"
]

random.seed(4)
random.shuffle(palette[9:len(palette)])

def getFeaturePos(clickedF, f_sorted, importance_matrix, M):
    kbar = np.sum(importance_matrix[0:M, ] != 0)/M
    for i in range(M):
        current_sum = 0
        selected_i = set(np.where(importance_matrix[i, :] > 0)[0])
        if clickedF in selected_i:
            for f in f_sorted:
                if f == clickedF:
                    return (current_sum + importance_matrix[i, f]/2)*100/kbar, i + 1/2
                if f in selected_i:
                    current_sum += importance_matrix[i, f]


def getClickedFeature(vec, imp):
    current_sum = 0
    for i in range(len(vec)):
        if current_sum + vec[i] >= imp:
            return i
        current_sum += vec[i]


grays = []
text = None
annot = None
currentClicked = -1
currentRun = -1
counterHover = 0


def plotMap(data, space, comp="", funMapping=None):
    try:
        if space == "functions":
            data = data["functions"]
        attrName = funMapping if space == "functions" else space
        importance_matrix, stabs, acc, names, msiData, pfs, totImportance, M, d, process_matrix, ensembl_names = \
            parseData(data, attrName, comp, funMapping, space == "functions")
        kbar = np.sum(importance_matrix[0:M, ] != 0)/M

        window = tk.Tk()
        bottomframe = tk.Frame(window)
        bottomframe.pack(side=tk.BOTTOM)
        label = tk.Label(bottomframe, text='Search', width=7, font=("Arial",22))
        label.pack(side=tk.LEFT, padx=0, pady=5)
        entry = tk.Entry(bottomframe)
        entry.pack(side=tk.LEFT, padx=5, pady=5)

        fig = Figure(figsize=(15, 15), dpi=100)
        ax = fig.add_subplot(111)
        ax.set(xlim=(0, 100), ylim=(0, M))

        ax.set_title("Feature Stability Map   (phi=" + str(round(stabs["phi"], ndigits=3)) + ", iw=" + str(round(stabs["iw"], ndigits=3)) +
                     ", msi=" + str(round(stabs["msis"][comp], ndigits=3)) + ", acc=" + acc + ")", fontsize=20)
        ax.set_ylabel("run", fontsize=18)
        ax.set_xlabel("Cumulative importance (%)", fontsize=18)
        ax.tick_params(axis='both', labelsize=18)

        canvas = FigureCanvasTkAgg(fig, master=window)
        canvas.get_tk_widget().pack(side=tk.TOP)

        occurrences = np.zeros(d)
        variances = np.zeros(d)
        for f in range(importance_matrix.shape[1]):
            occurrences[f] = np.sum(importance_matrix[:, f] != 0)
            if occurrences[f] > 0:
                variances[f] = np.std(importance_matrix[importance_matrix[:, f] != 0, f])/np.mean(importance_matrix[importance_matrix[:, f] != 0, f])
        fracs = 10 if np.sum(occurrences) < 1000 else 1
        vals = -occurrences+0.00000001*variances
        f_sorted = np.argsort(vals)
        colors = np.tile(palette, int(len(f_sorted)/len(palette)) + 1)
        _, _, map = redrawMap(f_sorted, ax, importance_matrix, None, None, 1, occurrences, msiData, M)

        def onclick(event):
            global grays, text
            if event.inaxes is not None and abs(event.inaxes.dataLim.y1-M) < 0.1:
                run = int(event.ydata)
                clickedIndex = getClickedFeature(importance_matrix[run, f_sorted], event.xdata * kbar / 100.0)
                if clickedIndex is not None:
                    for rect in grays:
                        rect.remove()
                    clickedF = f_sorted[clickedIndex]
                    if ensembl_names is not None:
                      webbrowser.open('https://www.genecards.org/Search/Keyword?queryString=' + ensembl_names[clickedF])
                    #print("CLICKED FEATURE: " + str(clickedF) + ", ON RUN: " + str(run))
                    grays, contribution, _ = redrawMap(f_sorted, ax, importance_matrix, run, clickedF, 1.0 / fracs, occurrences, msiData, M)
                    canvas.draw()
                    for fraction in np.arange(2, fracs+1):
                        for i in range(len(grays)-2):
                            rect = grays[i]
                            rect.set_width(rect.get_width()*fraction/(fraction-1.0))
                        grays[len(grays)-2].set_width(grays[len(grays)-2].get_width()/(1-(fraction-1)/fracs)*(1 - (fraction/fracs)))
                        canvas.draw()
                    fig2 = Figure(figsize=(18, 18), dpi=100)
                    ax2 = fig2.add_subplot(111)
                    ax2.set_title("Detailed stats on " + names[clickedF], fontsize=20)
                    order = np.argsort(-msiData["contributions_feat"][clickedF, f_sorted])

                    def my_autopct(pct):
                        return ('%1.1f%%' % pct) if pct >= 2.5 else ('%1.1f' % pct)

                    def my_autopct2(p):
                        return ('%1.1f' % p) if p <= 3.5 else ('%1.1f%%' % p)
                    shared_feat = msiData["shared_feat"]
                    contributions_feat = msiData["contributions_feat"]
                    ax2.pie(100*shared_feat[clickedF, f_sorted][order]/np.sum(shared_feat[clickedF, :]), labels=np.array(names)[f_sorted][order], counterclock=False,
                            colors=colors[order], autopct=my_autopct, pctdistance=0.85, textprops={'fontsize': 14}, radius=1, wedgeprops=dict(width=0.3, edgecolor=matplotlib.colors.colorConverter.to_rgba('black', alpha=.2)))
                    patches = ax2.pie(contributions_feat[clickedF, f_sorted][order] / np.sum(shared_feat[clickedF, :]), autopct=my_autopct2, pctdistance=0.8,
                            colors=colors[order], radius=0.7, wedgeprops=dict(width=0.3, edgecolor=matplotlib.colors.colorConverter.to_rgba('black', alpha=.2)), counterclock=False)[0]
                    for p in patches:
                      p.set_hatch('/')
                    #ax2.text(-0.2375, -0.2, s=r'$\phi_f=$' + str(round(100*np.sum(contributions_feat[clickedF, :])/(stabs["msis"][comp]*kbar), ndigits=3)) + "%" +
                    #                         " (" + str(round(rankdata(-1*msiData["sum_contributions"])[clickedF])) + ")"
                  #           , fontdict={'fontsize': 14})
                    ax2.text(-0.225, -0.1, s=r'$I_f=$' + str(
                        round(100 * np.sum(importance_matrix[:, clickedF]) / np.sum(importance_matrix[:, :]),
                              ndigits=3)) + "%" + " (" + str(round(rankdata(-1*totImportance)[clickedF])) + ")"
                             , fontdict={'fontsize': 14})

                    ax2.text(-0.1875, 0.1, s=r'$p_f=$' + str(round(pfs[clickedF],ndigits=3)) + " (" +
                             str(round(rankdata(-1*pfs)[clickedF])) + ")", fontdict={'fontsize': 14})

                    canvas2 = FigureCanvasTkAgg(fig2, master=tk.Tk())
                    canvas2.get_tk_widget().pack()
                    canvas2.draw()

        def subHover(xdata, ydata, inaxes, go=False):
            sys.stdout.flush()
            sys.stderr.flush()
            global annot, currentClicked, currentRun, counterHover
            counterHover += 1
            if xdata is not None and counterHover % 1 == 0 and (go or abs(inaxes.dataLim.y1 - M) < 0.1):
                run = int(ydata)
                clickedIndex = getClickedFeature(importance_matrix[run, f_sorted], xdata * kbar / 100.0)
                if clickedIndex is not None:
                    clickedF = f_sorted[clickedIndex]
                    if clickedF != currentClicked or currentRun != run:
                        xCenter = (map[run][clickedF]["start"] + map[run][clickedF]["end"]) / 2
                        yCenter = run + 0.5
                        if annot is not None:
                            annot.remove()
                        if process_matrix is not None:
                            if len(process_matrix[run, clickedF]) == 1:
                                textAnnot = names[clickedF] + "/"
                            else:
                                textAnnot = names[clickedF] + process_matrix[run, clickedF].replace("/","\n")
                        else:
                            textAnnot = names[clickedF]
                        xTextCenter = xCenter + 20 if xCenter < 60 else xCenter - 300
                        yTextCenter = yCenter + 20 if yCenter < M * 0.7 else yCenter - 100
                        annot = ax.annotate(textAnnot, xy=(xCenter, yCenter), xytext=(xTextCenter, yTextCenter),
                                                textcoords="offset points", size=14,
                                                bbox=dict(boxstyle="round", fc="w"),
                                                arrowprops=dict(arrowstyle="->"))

                        currentClicked = clickedF
                        currentRun = run
                        canvas.draw()
            if annot is not None and xdata is None and annot.get_text() != "":
                annot.set_visible(False)
                annot.set_text("")
                canvas.draw()

        def hover(event):
            subHover(event.xdata, event.ydata, event.inaxes)

        fig.canvas.mpl_connect("motion_notify_event", hover)
        fig.canvas.mpl_connect('button_press_event', onclick)

        def handle_click(event):
            info = entry.get()
            clickedF = names.index(info)
            x, y = getFeaturePos(clickedF, f_sorted, importance_matrix, M)
            subHover(x, y, None, True)

        entry.bind("<1>", handle_click)

        window.mainloop()
        canvas.draw()
    except tk.TclError as e:
        # normal keyboard interrupt
        if "invalid command name" not in e.args[0]:
            raise e


def redrawMap(f_sorted, ax, importance_matrix, clickedRun, clickedF, fraction, occurrences, msiData, M):
    kbar = np.sum(importance_matrix[0:M, ] != 0)/M
    clicked_x = 0
    grayRects = []
    contribution = 0
    map = []
    shared_matrix, sim_matrix = msiData["sharedMatrix"], msiData["simMatrix"]
    for i in range(M):
        map.append({})
        current_x = 0
        index = 0
        selected_i = set(np.where(importance_matrix[i,:]>0)[0])
        if len(selected_i) == 0:
            rect = patches.Rectangle((0, i), 100, 1, linewidth=1, edgecolor="black", facecolor="white")
            ax.add_patch(rect)
        else:
            for f in f_sorted:
                if f in selected_i:
                    # Create a Rectangle patch
                    if clickedRun is None:
                        rect = patches.Rectangle((current_x, i), importance_matrix[i, f] * 100 / kbar, 1, linewidth=1,
                                                 edgecolor="black",
                                                 facecolor=palette[index % len(palette)])
                        # Add the patch to the Axes
                        ax.add_patch(rect)
                        map[i][f] = {"start": current_x, "end": current_x + importance_matrix[i, f] * 100 / kbar}

                    if f == clickedF and i == clickedRun:
                        clicked_x = current_x

                    if clickedRun is not None and clickedF is not None and i != clickedRun:

                        minRun = min(clickedRun, i)
                        if minRun == i:
                            sharedVal = shared_matrix[i, clickedRun, f, clickedF]
                        else:
                            sharedVal = shared_matrix[clickedRun, i, clickedF, f]

                        if sharedVal != 0:
                            rect = patches.Rectangle((current_x, i), sharedVal * fraction * 100 / kbar, 1, linewidth=1,
                                                     edgecolor="black",
                                                     facecolor="gray", alpha=0.2)
                            ax.add_patch(rect)
                            grayRects.append(rect)

                            rect = patches.Rectangle((current_x, i), sharedVal * fraction *
                                                     sim_matrix[clickedF, f] * 100 / kbar, 1, linewidth=1,
                                                     edgecolor="black",
                                                     facecolor="gray", alpha=0.2, hatch="/")
                            ax.add_patch(rect)
                            grayRects.append(rect)
                            contribution += sharedVal/kbar * sim_matrix[clickedF, f]/(M-1)

                    current_x += importance_matrix[i, f] * 100 / kbar
                index += 1

    if clickedRun and clickedF:
        rect = patches.Rectangle((clicked_x, clickedRun), importance_matrix[clickedRun, clickedF] * (1-fraction) * 100 / kbar, 1,
                                 linewidth=1,
                                 edgecolor="none",
                                 facecolor="gray", alpha=0.2)
        ax.add_patch(rect)
        grayRects.append(rect)
        rect = patches.Rectangle((clicked_x, clickedRun), importance_matrix[clickedRun, clickedF] * 100 / kbar, 1,
                                 linewidth=1,
                                 edgecolor="red",
                                 facecolor="none")
        ax.add_patch(rect)
        grayRects.append(rect)
        return grayRects, contribution/(importance_matrix[clickedRun,clickedF]/kbar), map
    return grayRects, None, map


def loadShared(data, M, d):
    for m1 in data.keys():
        for m2 in data[m1].keys():
            if "float" in str(type(data[m1][m2]["feats1"])):
                data[m1][m2]["feats1"] = [data[m1][m2]["feats1"]]
            if "float" in str(type(data[m1][m2]["feats2"])):
                data[m1][m2]["feats2"] = [data[m1][m2]["feats2"]]
            data[m1][m2]["shared"] = np.array(data[m1][m2]["shared"]).reshape(
                (len(data[m1][m2]["feats1"]), len(data[m1][m2]["feats2"])))

    dic = {}
    for i in range(M):
        if i < M-1:
            for j in np.arange(i+1, M):
                feats1 = data[str(i)][str(j)]["feats1"]
                feats2 = data[str(i)][str(j)]["feats2"]
                for index1 in range(len(feats1)):
                    for index2 in range(len(feats2)):
                        sys.stdout.flush()
                        if data[str(i)][str(j)]["shared"][index1,index2] != 0:
                            dic[(i, j, int(feats1[index1]), int(feats2[index2]))] = data[str(i)][str(j)]["shared"][index1, index2]
                # ensures that the sparse matrix has the correct number of dimensions
                if (i, j, d-1, d-1) not in dic.keys():
                    dic[(i, j, d-1, d-1)] = 0
    return sparse.COO(dic)


def parseData(data, space, comp, processLoc, functional):
    spaceData = data[space]
    M, d = spaceData["importance_matrix"].shape
    msiData = {
        "simMatrix": spaceData["similarity_matrices"][comp],
        "sharedMatrix": loadShared(spaceData["shared_matrices"][comp], M, d),
        "shared_feat": np.zeros((d,d))
    }
    for i in range(M - 1):
        for j in range(M):
            if j != i:
                msiData["shared_feat"] += np.array(msiData["sharedMatrix"][i, j, :, :].todense()) + \
                                          np.transpose(msiData["sharedMatrix"][i, j, :, :].todense())

    msiData["shared_feat"] /= (M * (M - 1))
    msiData["contributions_feat"] = msiData["shared_feat"] * msiData["simMatrix"]
    msiData["sum_contributions"] = np.array([np.sum(msiData["contributions_feat"][f, :]) for f in range(d)])

    pfs, totImportance = np.zeros(d), np.zeros(d)

    for f in range(d):
        k = np.sum(spaceData["importance_matrix"][0:M, ] != 0) / M
        pfs[f] = np.mean(spaceData["importance_matrix"][:, f] > 0)
        totImportance[f] = np.mean(spaceData["importance_matrix"][:, f]) / k

    process_matrix = data[processLoc]["process_mat2"] if functional else \
        (data["functions"][processLoc]["process_mat"] if processLoc is not None else None)

    if isinstance(spaceData["accuracy"], (int, float)):
        spaceData["accuracy"] = str(round(spaceData["accuracy"], ndigits=3))

    return spaceData["importance_matrix"], spaceData["stabilities"], spaceData["accuracy"], spaceData["names"], msiData, pfs, totImportance, M, d, process_matrix, spaceData["ensembl_names"]



